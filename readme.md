# norshe - nordvpn helper script
finds the optimal nordvpn server and connects to it

### prerequisites:
- openvpn
- sed
- a nordvpn account
- a POSIX shell
- sudo (reccomended)

### usage:
- create an openvpn auth file (this is a text file with your 
username on the first line and password on the next)
    - the script expects this file to be at $HOME/.vpnauth
    - this can be modified through the auth_file variable near at top 
      of the script
- `sh norshe.sh`
- you are finished

### options:
- `norshe.sh udp` to use udp config files (default)
- `norshe.sh tcp` to use tcp config files
- `norshe.sh top[n]` pick from a list of the top n closest servers,
eg. `norshe.sh top3`
- invalid options are ignored

### notes:
- it seems like nordvpn limits top[n] to 5,
but you can try to go higher if you like
- you can specify udp and tcp at the same time, but this is silly
- you can change the following at the top of the script:
	- the location of your openvpn auth file
	- the openvpn command to be used (with options)
	- the su command to use (default sudo)
- you may also change any other variable in the script, but this 
is not reccomended
- if you would not like to store your nordvpn credentials in
plaintext, you can use the inorshe.sh script instead, and you
will be prompted for your credentials when openvpn starts
- i think some shells might not pass arguments correctly
when evaluating a text file, if this happens try setting the 
script executable (`chmod +x norshe.sh`) and running norshe directly as ./norshe.sh

(c) 2019 oneseveneight/sose
