#!/bin/sh

# norshe - nordvpn helper script
# Copyright (C) 2019 oneseveneight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

auth_file="$HOME/.vpnauth"
openvpn_command='openvpn --daemon' # command to be used for openvpn, with options
su="sudo" # su command for openvpn, can be blank if root

# TODO:
# option to keep vpn configs
# option to only get config
# traps

help_text="usage:
	norshe.sh [options]
options:
	help - display this text
	udp - uses udp configs provided by nordvpn (default)
	tcp - uses tcp configs provided by nordvpn
	top[n] - pick from a list of the top n closest servers instead of using the closest
	eg. \`norshe.sh top3\`"

find_nearest_servers() {
	local servers_to_find="$1"
	local server_reccomend_url="https://nordvpn.com/wp-admin/admin-ajax.php?action=servers_recommendations"
	local server_list_file="$(mktemp)" # json file obtained from nordvpn
	local server_list="" # list of server hostnames

	if ! curl -sf "$server_reccomend_url" > "$server_list_file"
	then
		echo "error: curl tried to fetch \"$download_url\" and failed"
		return 1
	fi

	# '}}]} marks the end of one server definition in the json file
	# we use this to split the file into lines to grep later
	sed -ie 's/}}]}/\n/g' "$server_list_file"
	echo "$server_list" | grep -vo "[0-9]"

	# we can use the fact that the 'hostname' key is always followed by
	# the 'load' key to find the hostname in the json using grep

	server_list="$(grep \
			-Eom "$servers_to_find" '"hostname":".*","load' "$server_list_file" \
			| cut -f 4 -d '"')"
	echo "$server_list"
	rm "$server_list_file"
}

download_config() {
	local config_name="$1" # name given by nordvpn to the config
	local config_file="$2" # file the config is written to
	local dowload_url="$3" # where to download the config files from
	
	echo "dowloading config $config_name..."

	download_url="$(echo "$download_url" \
			| sed -e "s/<configname>/$config_name/g")"
	if ! curl -sf "$download_url" > "$config_file" 
	then
		echo "error: curl tried to fetch \"$download_url\" and failed"
		return 1
	fi
}

connect_to_vpn() {
	# allows the user to select from a list of nearest servers
	local download_url="$1"
	local servers_to_find="$2"
	local vpn_config=""
	local server_list=""
	local user_selection=""
	local selected_server=""
	
	echo "finding nearest server..."
	if [ -z "$servers_to_find" ]
	then
		selected_server="$(find_nearest_servers 1)"
	else
		server_list="$(find_nearest_servers $servers_to_find)"
		numbered_server_list="$(echo "$server_list" | cat -n)" # numbers output lines
		echo "here are the $servers_to_find servers closest to you:"
		echo "$numbered_server_list"
		echo "which one would you like? (enter a number)"

		# cat uses a very specific padding for its line numbers, so it's important
		# that the string used by grep below consists of:
		# 5 spaces
		# the `user_selection` variable
		# and 1 horizontal tab
		# in that order and no other

		while ! echo "$numbered_server_list" | grep -q "     $user_selection	"
		do
			read user_selection
		done
		selected_server="$(echo "$server_list" |sed -n "$user_selection"p)"
	fi
	vpn_config="$(mktemp)"
	download_config "$selected_server" "$vpn_config" "$download_url"
	echo "starting openvpn, sudo may ask you for your password"
	openvpn_command="$su $openvpn_command --config "$vpn_config" --auth-user-pass "$auth_file""
	eval "$openvpn_command"
	rm "$vpn_config"
	echo "all done!"
}


get_options() {
	local available_options="help udp tcp quiet top[0-9]"
	local top_x="" # the 'n' in the 'top[n]' option
	local download_url="https://downloads.nordcdn.com/configs/files/ovpn_legacy/servers/<configname>.udp1194.ovpn"
	if ! [ -f "$auth_file" ]; then echo "could not find $auth_file" && exit; fi

	if [ -z "$1" ]
	then
		connect_to_vpn "$download_url"
		exit
	fi

	for option in $available_options
	do
		if echo "$@" | grep -qE "$option"
		then
			case "$option" in "help")
				echo "$help_text"
				exit
				;;
			"udp")
				echo "using udp config"
				download_url="https://downloads.nordcdn.com/configs/files/ovpn_legacy/servers/<configname>.udp1194.ovpn"
				;;
			"tcp")
				echo "using tcp config"
				download_url="https://downloads.nordcdn.com/configs/files/ovpn_legacy/servers/<configname>.tcp443.ovpn"
				;;
			"top[0-9]")
				top_x="$(echo "$@" | grep -E "$option" | grep -Eo "[0-9]")"
				;;
			"*")
				echo "the string '$@' does not consist of valid options"
				;;
			esac
		fi
	done
	connect_to_vpn "$download_url" "$top_x"
}

get_options $@
